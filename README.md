# Howland Current Pump
(ac current generator)

![](https://img.shields.io/badge/V6-KiCad-blue)
Simulation is currently not compatible with KiCAD 7
![schematic](images/ac_current_generator.jpg)

## Video with the circuit description

https://youtu.be/Bf4n697qHjU


## Description
This repo is the building block for KiCAD with Built in SPICE spice simulation. 
Circuit used in this building block is called the Howland Current Pump (first published in 1964).
For the theory of operation refer to the linked [TI application note](https://www.ti.com/lit/an/snoa474a/snoa474a.pdf?ts=1698859958003). Refer to Multi-Range Current Pump chapter.
This circuit gives the MCU an abilty to generate AC current with required amplitude using only the DAC and 2 precision opamps.


## Usage
Insparation is that you can copy the repo and get a reusable circuit block. 

## Support
Please, contact me if you need any help <br>
Telegramm: https://t.me/Plinsboorg <br>
Email: dennis.yaskevich@proton.me <br>
I do commercial electronics design and consulting. <br>
I can also help you for free if you're doing an open hardware project.<br>

## Roadmap
I'll continue adding intresting analog circuits as separate repos

## Contributing
Please, submit your fixes if you see any mistakes or possible improvments.

## Authors and acknowledgment
This circuit is inspired by Flexi-TEER project
https://github.com/mdanderson03/Flexi-TEER

## License
[Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)
